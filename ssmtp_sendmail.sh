#!/bin/bash

I=`dpkg -s ssmtp | grep "Status" `
if [ -n "$I" ] #проверяем что нашли строку со статусом (что строка не пуста)
then
   echo "ssmtp installed" #выводим результат
else
    bash -c "sudo apt install ssmtp -y"
fi


A=`dpkg -s mailutils | grep "Status" `
if [ -n "$A" ] #проверяем что нашли строку со статусом (что строка не пуста)
then
   echo "mailutils installed" #выводим результат
else
    bash -c "sudo apt install mailutils -y"
fi

####################################################################################
read -p "Введите mail server: " mail
read -p "Введите hostname: " hostname
read -p "Введите user: " user
read -s -p "Введите password: "$'\n' password  echo

ssmtp=/etc/ssmtp/ssmtp.conf

cat<<EOF>$ssmtp
root=postmaster
mailhub=$mail:587
hostname=$hostname
AuthUser=$user
AuthPass=$password
FromLineOverride=YES
UseSTARTTLS=YES

EOF

####################################################################################
read -p "Введите text: " text
read -p "Введите title: " title
read -p "Введите komu poslat: " tomail
echo "$text" | mail -s "$title" $tomail
