#!/bin/bash

apt install sudo

wget -qO- https://repos.influxdata.com/influxdb.key | sudo tee /etc/apt/trusted.gpg.d/influxdb.asc >/dev/null

. /etc/os-release

echo "deb https://repos.influxdata.com/${ID} ${VERSION_CODENAME} stable" | sudo tee /etc/apt/sources.list.d/influxdb.list


sudo apt-get update && sudo apt-get install telegraf


apt install git -y

cd

rm -rf `pwd`/install-telegraf
git clone https://gitlab.com/abbaslifarid28/install-telegraf/

mv /etc/telegraf/telegraf.conf /etc/telegraf/telegraf.conf.back1


db=$(hostname) 


cd install-telegraf 

cp telegraf.conf /etc/telegraf/
cp smart.conf /etc/telegraf/telegraf.d/
cp zpool_influxdb /usr/lib/zfs-linux/
cp smart /etc/sudoers.d/

sed -i  "131 s/proxmox/$db/" /etc/telegraf/telegraf.conf
var=$(smartctl --scan -d scsi|cut -f1 -d\#|sed  's/ $/\", /;s/^/\"/'|tr -d '\n' )
 echo devices = [ $var ] >> /etc/telegraf/telegraf.d/smart.conf


apt-get install lm-sensors -y

systemctl restart telegraf

systemctl restart pvestatd


