apt install git -y
rm -rf `pwd`/install-telegraf
git clone https://gitlab.com/abbaslifarid28/install-telegraf/

mv /etc/telegraf/telegraf.conf /etc/telegraf/telegraf.conf.back1


db=$(hostname) 


cd install-telegraf 

cp telegraf.conf /etc/telegraf/
cp smart.conf /etc/telegraf/telegraf.d/
cp zpool_influxdb /usr/lib/zfs-linux/
cp smart /etc/sudoers.d/

sed -i  "131 s/proxmox/$db/" /etc/telegraf/telegraf.conf
var=$(smartctl --scan -d scsi|cut -f1 -d\#|sed  's/ $/\", /;s/^/\"/'|tr -d '\n' )
 echo devices = [ $var ] >> /etc/telegraf/telegraf.d/smart.conf

apt install sudo
apt-get install lm-sensors

systemctl restart telegraf

systemctl restart pvestatd
